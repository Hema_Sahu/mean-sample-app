 function AppCtrl($scope,$http) {
	console.log("Hello world from controller");

	var refresh=function(){
	$http.get('/userlist').success(function(response){
		console.log('I got the data i requested');
		$scope.userlist=response;
		$scope.user="";
	});
};

	refresh();

	$scope.adduser=function(){
		console.log($scope.user);
		$http.post('userlist',$scope.user).success(function(response){
			console.log(response);
			refresh();
		});
	};

	$scope.remove=function(id){
		console.log(id);
		$http.delete('/userlist/'+id).success(function(response){
			refresh();
		});
	}
	

	$scope.edit=function(id){
		console.log(id);
		$http.get('/userlist/'+id).success(function(response){
			$scope.user=response;
		});
	};

	$scope.update=function(){
		console.log($scope.user._id);
		$http.put('/userlist/'+$scope.user._id,$scope.user).success(function(response){
			refresh();
		})
	};
}